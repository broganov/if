using System;

namespace Condition
{
    public static class Condition
    {
        /// <summary>
        /// Implement code according to description of  task 1
        /// </summary>        
        public static int Task1(int n)
        {             
            if (n < 0) return Math.Abs(n);
            else if (n > 0) return n * n;
            else return 0;
        }

        /// <summary>
        /// Implement code according to description of  task 2
        /// </summary>  
        public static int Task2(int n)
        {
            int hundreds = n / 100;
            int tens = (n - hundreds * 100) / 10;
            int ones = n - hundreds * 100 - tens * 10;
            int max = Math.Max(hundreds, Math.Max(tens, ones));
            int min = Math.Min(hundreds, Math.Min(tens, ones));
            int middle;
            if (hundreds == max)
            {
                middle = Math.Max(ones, tens);
            }
            else if (tens == max)
            {
                middle = Math.Max(ones, hundreds);
            } 
            else
            {
                middle = Math.Max(tens, hundreds);
            }
            return 100 * max + 10 * middle + min;
        }
    }
}
